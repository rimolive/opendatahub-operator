---
apiVersion: v1
kind: Secret
metadata:
  annotations:
    template.openshift.io/expose-database_name: '{.data[''database-name'']}'
    template.openshift.io/expose-password: '{.data[''database-password'']}'
    template.openshift.io/expose-username: '{.data[''database-user'']}'
  name: {{ DATABASE_SERVICE_NAME }}
  namespace: {{ meta.namespace }}
stringData:
  database-name: {{ POSTGRESQL_DATABASE }}
  database-password: {{ POSTGRESQL_PASSWORD }}
  database-user: {{ POSTGRESQL_USER }}
---
apiVersion: v1
kind: Service
metadata:
  annotations:
    template.openshift.io/expose-uri: postgres://{.spec.clusterIP}:{.spec.ports[?(.name=="postgresql")].port}
  name: {{ DATABASE_SERVICE_NAME }}
  namespace: {{ meta.namespace }}
spec:
  ports:
  - name: postgresql
    port: 5432
    targetPort: 5432
    protocol: TCP
  type: ClusterIP
  selector:
    name: {{ DATABASE_SERVICE_NAME }}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ DATABASE_SERVICE_NAME }}
  namespace: {{ meta.namespace }}
spec:
  accessModes:
  - ReadWriteOnce
  storageClassName: {{ STORAGE_CLASS_NAME }}
  resources:
    requests:
      storage: {{ POSTGRES_VOLUME_CAPACITY }}
    limits:
      storage: {{ POSTGRES_VOLUME_CAPACITY }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    template.alpha.openshift.io/wait-for-ready: "true"
  name: {{ DATABASE_SERVICE_NAME }}
  namespace: {{ meta.namespace }}
spec:
  replicas: 1
  selector:
    matchLabels:
      name: {{ DATABASE_SERVICE_NAME }}
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        name: {{ DATABASE_SERVICE_NAME }}
    spec:
      containers:
      - env:
        - name: POSTGRESQL_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: {{ DATABASE_SERVICE_NAME }}
        - name: POSTGRESQL_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-password
              name: {{ DATABASE_SERVICE_NAME }}
        - name: POSTGRESQL_DATABASE
          valueFrom:
            secretKeyRef:
              key: database-name
              name: {{ DATABASE_SERVICE_NAME }}
        image: "quay.io/avsrivas/postgresql-96-centos7:9.6"
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - /usr/libexec/check-container
            - --live
          initialDelaySeconds: 120
          timeoutSeconds: 10
        name: postgresql
        ports:
        - containerPort: 5432
        readinessProbe:
          exec:
            command:
            - /usr/libexec/check-container
          initialDelaySeconds: 5
          timeoutSeconds: 1
        resources:
          limits:
            memory: {{ POSTGRES_MEMORY_LIMIT }}
        volumeMounts:
        - mountPath: /var/lib/pgsql/data
          name: "{{ DATABASE_SERVICE_NAME }}-data"
      volumes:
      - name: "{{ DATABASE_SERVICE_NAME }}-data"
        persistentVolumeClaim:
          claimName: {{ DATABASE_SERVICE_NAME }}
