Hue
===

Deploys the Cloudera Hue server to do Data Exploration with S3 buckets.

**NOTE:** This is a tech preview role.

Requirements
------------

- Spark Cluster
- Hive Metastore
- Spark SQL Thrift Server

Role Variables
--------------

* `user` - Sets the Hue DB user
* `password` - Sets the Hue DB password
* `root_password` - Sets the Hue DB root password
* `volume_capacity` - Sets the Hue DB Volume Capacity
* `image` - Sets the Hue DB image name
* `memory_limit` - Sets the Hue DB Memory limit

* `hue_blacklist` - Sets the Hue application blacklist
* `hue_secret_key` - Sets the Hue Secret Key

Dependencies
------------

Thrift Server

Sample Configuration
--------------------

```
hue:
  database:
    user: changeme
    password: changeme
    root_password: changeme
    volume_capacity: 10Gi
    image: "registry.redhat.io/rhscl/mysql-57-rhel7"
    memory_limit: 1Gi

  hue_blacklist: impala,security,jobbrowser,jobsub,pig,hbase,sqoop,zookeeper,spark,oozie,search

  hue_secret_key: changeme
```

License
-------

GNU GPLv3

Author Information
------------------

contributors@lists.opendatahub.io